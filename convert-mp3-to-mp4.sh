#!/bin/bash

# Script to convert multiple mp3 files to mp4 videos with a static image using FFmpeg
# Created by Fahad ALGhathbar

echo "Starting the conversion process..."

# إعداد المتغير الذي يحتوي على اسم الصورة الثابتة
static_image="photo.png"

# إعداد المتغير الذي يحتوي على التنسيق المستخدم للفيديو
video_format="yuv420p"

# متغير لترقيم ملفات الفيديو المُنشأة
i=0

# حلقة لتحويل كل ملف mp3 إلى فيديو mp4
for file in *.mp3; do
    i=$((i+1))
    output_video="${i}-${file%.mp3}.mp4"  # اسم الملف النهائي للفيديو

    # استخدام FFmpeg لإنشاء الفيديو
    ffmpeg -loop 1 -y -i "$static_image" -i "$file" -shortest -pix_fmt "$video_format" "$output_video"
done

echo "Conversion process completed successfully!"
